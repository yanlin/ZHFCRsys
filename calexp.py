#!/usr/bin/python2.6
import sys, os, commands
import shutil
import getpass
import argparse
cmd='root-config --libdir'
ROOTSYS=commands.getoutput(cmd)
sys.path.append(ROOTSYS)
import glob,fileinput, ROOT
from ROOT import TFile,TTree,TChain,TBranch,TH1,TH1F,TList,TH3,TH3F,TLine
from plotsys import plotvars
#parser = argparse.ArgumentParser()
#parser.add_argument("--inputFile",'-i', type=str, help="input CxAODs list", required=True)
#parser.add_argument("--process", '-p', type=str, help="process: ttbar, Ztautau, etc", required=True)
#parser.add_argument("--region", '-r', type=str, help="CR, SR, InvIso, etc", required=True)
#args = parser.parse_args()

#procdic = {'Zmm': 'Zbb', 'higgs_zz': 'HtoVV', 'fakes_top_stop': 'stopWt', 'fakes_Wtaunu': 'W', 'sig_nonres': 'hhWWbb', 'fakes_Wmunu': 'W', 'higgs_ww': 'HtoVV', 'stop': 'stopWt', 'ttbarV': 'ttbar', 'ttbar': 'ttbar', 'fakes_Wenu': 'W', 'higgs_bb': 'Hbb', 'diboson': 'ZZ', 'higgs_vh': 'VH', 'Ztt': 'Zttbb', 'fakes_diboson': 'ZZ', 'Zee': 'Zbb', 'data': 'data', 'higgs_ttH': 'ttH', 'fakes_top_ttbar': 'ttbar'}

procdic = {'diboson': 'ZZ', 'Zee_ew': 'DY', 'fakes_Wmunu': 'W', 'Zee_cc': 'Zcc', 'higgs_zz_vbf': 'HtoVV', 'ttbarV': 'ttbar', 'fakes_Wenu': 'W', 'Zee_bb': 'Zbb', 'Ztt_bb': 'Zttbb', 'higgs_zz_ggf': 'HtoVV', 'Ztt_cc': 'Zttcc', 'fakes_diboson': 'ZZ', 'fakes_top_stop': 'stopWt', 'fakes_Wtaunu': 'W', 'data': 'data', 'sig_nonres_bbtautau': 'hhttbb_bbll', 'higgs_ww_vbf': 'HtoVV', 'Ztt_ew': 'DYtt', 'Zmm_bb': 'Zbb', 'ttbar': 'ttbar', 'stop': 'stopWt', 'higgs_ww_ggf': 'HtoVV', 'sig_nonres_bbzz': 'hhZZbb', 'higgs_tautau_vh': 'VHtautau', 'higgs_tth': 'ttH', 'higgs_ww_wh': 'HtoVV', 'fakes_top_ttbar': 'ttbar', 'higgs_tautau_vbf': 'VBFHtautau', 'Zmm_ew': 'DY', 'Zmm_cc': 'Zcc', 'Zmm_ll': 'Zl', 'higgs_tautau_ggf': 'ggHtautau', 'Zee_ll': 'Zl', 'Ztt_ll': 'Zttl', 'sig_nonres_bbww': 'hhWWbb'}

#evts = {'diboson': 403.89998430013657, 'Zee_ew': 32.893917988520116, 'fakes_Wmunu': 9.228406626731157, 'Zee_cc': 1262.813585266471, 'higgs_zz_vbf': 0.6319045451236889, 'ttbarV': 79.54874241352081, 'fakes_Wenu': 9.905996441841125, 'Zee_bb': 18378.78688430786, 'Ztt_bb': 7.5190875441767275, 'higgs_zz_ggf': 0.5621235897997394, 'Ztt_cc': 2.266245849430561, 'fakes_diboson': 1.0868786741048098, 'fakes_top_stop': 16.0787014067173, 'fakes_Wtaunu': 1.1429222226142883, 'data': 96032.0, 'sig_nonres_bbtautau': 0.004088804435014026, 'higgs_ww_vbf': 0.007603311911225319, 'Ztt_ew': 0.051879432052373886, 'Zmm_bb': 24566.708263397217, 'ttbar': 34144.914794921875, 'stop': 1217.8306617736816, 'higgs_ww_ggf': 0.07951153127942234, 'sig_nonres_bbzz': 0.00549077505530704, 'higgs_tautau_vh': 0.26313768047839403, 'higgs_tth': 32.55177801847458, 'higgs_ww_wh': 0.05375837121391669, 'fakes_top_ttbar': 685.4218101501465, 'higgs_tautau_vbf': 0.04144674597773701, 'Zmm_ew': 41.94322998449206, 'Zmm_cc': 1813.468977034092, 'Zmm_ll': 296.0209973156452, 'higgs_tautau_ggf': 0.5692352590267546, 'Zee_ll': 457.7563500478864, 'Ztt_ll': 0.0, 'sig_nonres_bbww': 0.0008472898898617132} 

evts = {'diboson': 411.0373634696007, 'Zee_ew': 32.893917988520116, 'fakes_Wmunu': 8.832315284758806, 'Zee_cc': 1262.813585266471, 'higgs_zz_vbf': 0.6319045451236889, 'ttbarV': 437.65302991867065, 'fakes_Wenu': 9.928239326924086, 'Zee_bb': 18273.318382263184, 'Ztt_bb': 7.5190875441767275, 'higgs_zz_ggf': 0.5621235897997394, 'Ztt_cc': 2.266245849430561, 'fakes_diboson': 1.0293210626696236, 'fakes_top_stop': 16.360648185014725, 'fakes_Wtaunu': 1.3110725656151772, 'data': 96032.0, 'sig_nonres_bbtautau': 0.004088804435014026, 'higgs_ww_vbf': 0.007603311911225319, 'Ztt_ew': 0.051879432052373886, 'Zmm_bb': 24433.50454711914, 'ttbar': 33730.79870605469, 'stop': 1217.8306617736816, 'higgs_ww_ggf': 0.07951153127942234, 'sig_nonres_bbzz': 0.00549077505530704, 'higgs_tautau_vh': 0.26313768047839403, 'higgs_tth': 32.5517098903656, 'higgs_ww_wh': 0.05375837121391669, 'fakes_top_ttbar': 718.0089979171753, 'higgs_tautau_vbf': 0.04144674597773701, 'Zmm_ew': 41.94322998449206, 'Zmm_cc': 1784.4503577947617, 'Zmm_ll': 294.62533308565617, 'higgs_tautau_ggf': 0.6006656477111392, 'Zee_ll': 446.93329928815365, 'Ztt_ll': 0.0, 'sig_nonres_bbww': 0.0008472898898617132}

def getproc():
	fin = open(sys.argv[1], "r")
        procs = []
	for line in fin.readlines():
                line = line.rstrip("\n")
		if "p4" in line: continue
                proc = line.split("/")[-1].split(".")[0]
		procs.append(proc)
	print procs	
	#procs = ['data', 'diboson', 'fakes_diboson', 'fakes_top_stop', 'fakes_top_ttbar', 'fakes_Wenu', 'fakes_Wmunu', 'fakes_Wtaunu', 'higgs_bb', 'higgs_ttH', 'higgs_vh', 'higgs_ww', 'higgs_zz', 'sig_nonres', 'stop', 'ttbar', 'ttbarV', 'Zee', 'Zmm', 'Ztt']
	procs = ['data', 'diboson', 'fakes_diboson', 'fakes_top_stop', 'fakes_top_ttbar', 'fakes_Wenu', 'fakes_Wmunu', 'fakes_Wtaunu', 'higgs_tautau_ggf', 'higgs_tautau_vbf', 'higgs_tautau_vh', 'higgs_tth', 'higgs_ww_ggf', 'higgs_ww_vbf', 'higgs_ww_wh', 'higgs_zz_ggf', 'higgs_zz_vbf', 'sig_nonres_bbtautau', 'sig_nonres_bbww', 'sig_nonres_bbzz', 'stop', 'ttbarV', 'Zee_bb', 'Zee_cc', 'Zee_ew', 'Zee_ll', 'Zmm_bb', 'Zmm_cc', 'Zmm_ew', 'Zmm_ll', 'Ztt_bb', 'Ztt_cc', 'Ztt_ll', 'ttbar', 'Ztt_ew']

	names = ['data', 'ZZ', 'ZZ', 'stopWt', 'ttbar', 'W', 'W', 'W', 'ggHtautau', 'VBFHtautau', 'VHtautau', 'ttH', 'HtoVV', 'HtoVV', 'HtoVV', 'HtoVV', 'HtoVV', 'hhttbb_bbll', 'hhWWbb', 'hhZZbb', 'stopWt', 'ttbar', 'Zbb', 'Zcc', 'DY', 'Zl', 'Zbb', 'Zcc', 'DY', 'Zl', 'Zttbb', 'Zttcc', 'Zttl', 'ttbar', 'DYtt']

	print len(procs), len(names)
	dic = dict(zip(procs, names))
	print dic

def getwtkeys():
        '''
	lists = []
	#line = "/eos/home-b/brottler/bbll/fit_input/200827/ZllCR/Zee_bb.root"
	line = "/eos/home-b/brottler/bbll/fit_input/201112/ZllCR/Zee_bb.root"
	rtfile = TFile(line)
	tree = rtfile.Get("Zee_bb")
	branchList = tree.GetListOfBranches()
        nBranch = tree.GetNbranches()
	#print branchList, nBranch
	for i in range(nBranch):
		varname = branchList.At(i).GetName()
		#if "up" in varname or "down" in varname: lists.append(varname)
		if "up" in varname:
			varname = varname[:-3]
			lists.append(varname)
	print lists, len(lists)
        '''
	lists = ['lepton_sf_el_eff_id', 'lepton_sf_el_eff_iso', 'lepton_sf_el_eff_reco', 'lepton_sf_muon_eff_iso_stat', 'lepton_sf_muon_eff_iso_sys', 'lepton_sf_muon_eff_reco_stat', 'lepton_sf_muon_eff_reco_sys', 'lepton_sf_muon_eff_reco_stat_lowpt', 'lepton_sf_muon_eff_reco_sys_lowpt', 'jet_jvt_efficency', 'prw_datasf', 'trigger_sf_el_eff_trigger', 'trigger_sf_nom_muon_eff_trig_stat', 'trigger_sf_nom_muon_eff_trig_sys', 'btag_sf_eff_eigen_b_0', 'btag_sf_eff_eigen_b_1', 'btag_sf_eff_eigen_b_2', 'btag_sf_eff_eigen_c_0', 'btag_sf_eff_eigen_c_1', 'btag_sf_eff_eigen_c_2', 'btag_sf_eff_eigen_c_3', 'btag_sf_eff_eigen_light_0', 'btag_sf_eff_eigen_light_1', 'btag_sf_eff_eigen_light_2', 'btag_sf_eff_eigen_light_3', 'btag_sf_eff_eigen_extrapolation', 'btag_sf_eff_eigen_extrapolation_from_charm']
	naming = ['EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR', 'EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR', 'EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR', 'MUON_EFF_ISO_STAT', 'MUON_EFF_ISO_SYS', 'MUON_EFF_RECO_STAT', 'MUON_EFF_RECO_SYS', 'MUON_EFF_RECO_STAT_LOWPT', 'MUON_EFF_RECO_SYS_LOWPT', 'JET_JvtEfficiency', 'PRW_DATASF', 'EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR', 'MUON_EFF_TrigStatUncertainty', 'MUON_EFF_TrigSystUncertainty', 'FT_EFF_Eigen_B_0', 'FT_EFF_Eigen_B_1', 'FT_EFF_Eigen_B_2', 'FT_EFF_Eigen_C_0', 'FT_EFF_Eigen_C_1', 'FT_EFF_Eigen_C_2', 'FT_EFF_Eigen_C_3', 'FT_EFF_Eigen_Light_0', 'FT_EFF_Eigen_Light_1', 'FT_EFF_Eigen_Light_2', 'FT_EFF_Eigen_Light_3', 'FT_EFF_extrapolation', 'FT_EFF_extrapolation_from_charm']
	print len(lists), len(naming)
	dic = dict(zip(lists, naming))
	print dic
	return dic

def getevtsys():
	#line = "/eos/home-b/brottler/bbll/fit_input/200713/ZllCR/Zmm_p4.root"
	line = "/eos/atlas/user/y/yanlin/bbllNtuples/ttbar_p4.root"
	#line = "/eos/home-b/brottler/bbll/fit_input/200827/ZllCR/Zmm_cc_p4.root"
	#line = "/eos/home-b/brottler/bbll/fit_input/200827/ZllCR/fakes_top_stop_p4.root"
	rtfile = TFile(line)
	keys = []
	for k in rtfile.GetListOfKeys():
		keyname = k.GetName().split("ttbar_")[1]
		#keyname = k.GetName().split("fakes_top_stop_")[1]
        	if k.GetClassName() == 'TTree': keys.append(keyname)
	print keys
	print len(keys)
	return keys	

def getsyskeys(line, proc):
	rtfile = TFile(line)
	keys = []
	for k in rtfile.GetListOfKeys():
		keyname = k.GetName().split("{0}_".format(proc))[1]
		if k.GetClassName() == 'TTree': keys.append(keyname)
	print keys
        print len(keys)
        return keys
	
def getttbarkeys():
	line = "/eos/home-b/brottler/bbll/fit_input/200713/ZllCR/ttbar.root"
	rtfile = TFile(line)
        tree = rtfile.Get("Nominal")
        branchList = tree.GetListOfBranches()
        nBranch = tree.GetNbranches()
	for i in range(nBranch):
		varname = branchList.At(i).GetName()
		if "mcweight_ttbar_" not in varname: continue
		key = varname.split("mcweight_ttbar_")[1]
		print key

def ini_three_hist():
	h1 = TH1F("h1", "h1", 35, 75, 110)
        h2 = TH1F("h2", "h2", 35, 75, 110)
        h3 = TH1F("h3", "h3", 35, 75, 110)
        h1.Sumw2()
        h2.Sumw2()
        h3.Sumw2()
	return h1, h2, h3

def hist_list(h1, h2, h3):
        h_list = []
        h_clone = []
	h_list.append(h2)
        h_list.append(h3)
        h_sys2 = h2.Clone()
        h_sys3 = h3.Clone()
        h_clone.append(h_sys2)
        h_clone.append(h_sys3)
	return h_list, h_clone

def calvar(h1, h2, h3):
	up = h2.Integral()/h1.Integral() - 1
	dn = h3.Integral()/h1.Integral() - 1
	return up, dn

def fillweightsys(key, tree):
	h1 = TH1F("h1", "h1", 35, 75, 110)
	h2 = TH1F("h2", "h2", 35, 75, 110)
	h1.Sumw2()
	h2.Sumw2()
	#h1 = h.Clone("h1_{0}".format(key))
	#h2 = h.Clone("h2_{0}".format(key))
	if "lepton_sf" in key:
		tree.Draw("ll_m>>h1", "weight*{0}_up/lepton_sf_nom".format(key))
		tree.Draw("ll_m>>h2", "weight*{0}_down/lepton_sf_nom".format(key))
	elif "jet_jvt" in key:
		tree.Draw("ll_m>>h1", "weight*{0}_up/jet_jvt_efficency_nom".format(key))
		tree.Draw("ll_m>>h2", "weight*{0}_down/jet_jvt_efficency_nom".format(key))
	elif "prw_" in key:
		tree.Draw("ll_m>>h1", "weight*{0}_up/prw_nom".format(key))
		tree.Draw("ll_m>>h2", "weight*{0}_down/prw_nom".format(key))
	elif "trigger_sf" in key:
		tree.Draw("ll_m>>h1", "weight*{0}_up/trigger_sf_nom".format(key))
		tree.Draw("ll_m>>h2", "weight*{0}_down/trigger_sf_nom".format(key))
	elif "btag_sf" in key:
		tree.Draw("ll_m>>h1", "weight*{0}_up/btag_sf_nom".format(key))
		tree.Draw("ll_m>>h2", "weight*{0}_down/btag_sf_nom".format(key))
	print "{0}: {1}, {2}".format(key, h1.Integral(), h2.Integral())
	return h1, h2

def checknom():
	fin = open(sys.argv[1], "r")
	h = TH1F("h", "h", 35, 75, 110)
        h.Sumw2()
	for line in fin.readlines():
		if "p4" not in line: continue
		line = line.rstrip("\n")
		proc = line.split("/")[-1].split(".")[0][:-3]
		rtfile = TFile(line)
	  	tree = rtfile.Get("{0}_Nominal".format(proc))
		h1 = h.Clone("h1")
                tree.Draw("ll_m>>h1", "weight")
		value = h1.Integral()
		nom = evts[proc]
		if nom != 0: print proc, nom, value, abs(value/nom-1)*100
		else: print proc, nom, value

def fillevtsys():
	print sys.argv[1]
        fin = open(sys.argv[1], "r")
	h = TH1F("h", "h", 35, 75, 110)
        h.Sumw2()
	for line in fin.readlines():
		if "p4" not in line: continue
                line = line.rstrip("\n")
                proc = line.split("/")[-1].split(".")[0][:-3]
		syskeys = getsyskeys(line, proc)
	        scale = 1
		#if proc == "higgs_tth": scale = 0.98447476
		#if proc == "sig_nonres_bbtautau": scale = 0.55783958
		#if proc == "sig_nonres_bbww": scale = 1.7926301
	 	print proc, scale	 
		name = procdic[proc]
		rtfile = TFile(line)
		nomtree = rtfile.Get("{0}_Nominal".format(proc))
                outfile = TFile("outfiles/0424_{0}_evtsys.root".format(proc), "recreate")
                outfile.mkdir("Systematics")
		for key in syskeys:
			if "JERPD" in key: continue
			if "Nominal" in key: continue
			#print key
			tree = rtfile.Get("{0}_{1}".format(proc, key))
			h1 = h.Clone("h1")
			tree.Draw("ll_m>>h1", "weight")
			h1.Scale(scale)
			if "JERMC" in key:
				h_pd = TH1F("h_pd", "h_pd", 35, 75, 110)
				h_pd.Sumw2()
				pd_key = key.replace("JERMC", "JERPD")
				pd_tree = rtfile.Get("{0}_{1}".format(proc, pd_key))
				pd_tree.Draw("ll_m>>h_pd", "weight")
				h_pd.Scale(scale)
                                h_pd.Add(h1, -1)
				#h1.Add(h_pd, -1)
				h_nom = TH1F("h_nom", "h_nom", 35, 75, 110)
                                h_nom.Sumw2()
				nomtree.Draw("ll_m>>h_nom", "weight")
				h_nom.Scale(scale)
				#h1.Add(h_nom)
				h_pd.Add(h_nom)
				key = key.replace("JERMC", "JER")
				print key, h1.Integral(), h_pd.Integral(), h_nom.Integral()
			        outfile.cd("Systematics")
                                h_pd.Write("{0}_Sys{1}".format(name, key))
                        else:	
			        outfile.cd("Systematics")
			        h1.Write("{0}_Sys{1}".format(name, key))
		
def weightsys():
        evts = {}
	syskeys = getwtkeys()
	print sys.argv[1]
        fin = open(sys.argv[1], "r")
	h = TH1F("h", "h", 35, 75, 110)
        h.Sumw2()
        for line in fin.readlines():
		if "p4" in line: continue
                line = line.rstrip("\n")
                proc = line.split("/")[-1].split(".")[0]
		name = procdic[proc]
                rtfile = TFile(line)
		tree = rtfile.Get(proc)
		outfile = TFile("outfiles/0424_{0}_weightsys.root".format(proc), "recreate")
		h1 = h.Clone("h1")
                tree.Draw("ll_m>>h1", "weight")
		evts[proc] = h1.Integral()
		outfile.cd()
		h1.Write(name)
		if "data" in line: continue
		outfile.mkdir("Systematics")
		for key in syskeys:
			#print key
			h_up, h_down = fillweightsys(key, tree)
			#h_up, h_down = fillweightsys("btag_sf_eff_eigen_b_6", tree)
		        outfile.cd("Systematics")
			h_up.Write("{0}_Sys{1}__1up".format(name, syskeys[key]))
			h_down.Write("{0}_Sys{1}__1down".format(name, syskeys[key]))
	print evts

			
			
def nominal():
	h = TH1F("h", "h", 35, 75, 110)
	fin = open(args.inputFile, "r")
	for line in fin.readlines():
		line = line.rstrip("\n")
		proc = line.split("/")[-1].split(".")[0]
		rtfile = TFile(line)
		outfile = TFile("{0}.root".format(proc), "recreate")
		tree = rtfile.Get("Nominal")
		h1 = h.Clone("h1")
		tree.Draw("ll_m>>h1", "weight")
		outfile.cd()
		h1.Write(proc)
		
	
if __name__ == "__main__":
    #main()
    #checknom()
    #getproc()
    #getwtkeys()
    weightsys()
    #getttbarkeys()
    #fillevtsys()
    #getevtsys()
